# This library Dockerfile builds an NodeJS+TypeScript-based service with common libraries
# Your module must comply with the following:
# * have a package.json and yarn.lock.json
# * have a test npm script entry in package.json
# * place all your tests under `unit-tests` directory
# * have a build npm script entry in package.json
# * have a tslint.json and tsconfig.json configuration files
# * output the build result to the /app/build folder containing an App.js main code file.

# Start off by bringing Lightflow's nodejs build:
FROM olympia.azurecr.io/lf-node:1.0.0 as baseBuilder
FROM olympia.azurecr.io/lf-node-runner:1.0.0 as baseRunner

############## Compiler stage:

# Reuse the base image which has dependencies installed already
FROM baseBuilder as builder

# install native node_modules dependencies:
RUN apt-get update \
  && \
  apt-get install -y \
  libsecret-1-0

# Install Nodejs dependencies:
ADD .npmrc ./iris.npmrc
COPY *.npmrc ./
RUN cat manual.npmrc > .npmrc || true; echo >> .npmrc; cat iris.npmrc >> .npmrc
RUN cat .npmrc
ADD package.json .
ADD yarn.lock .
RUN yarn install --frozen-lockfile

# Configure TypeScript:
ADD tslint.json .
ADD tsconfig.json .

# Add test code and the bulk of source code:
ADD src/ src/
ADD unit-tests/ unit-tests/

# Build documentation

# Test and build:
RUN yarn test
RUN yarn build

############## Clean production node_modules install:
FROM baseBuilder as runnerBuilder

# install native node_modules dependencies:
RUN apt-get update \
  && \
  apt-get install -y \
  libsasl2-2 \
  libsasl2-modules \
  libsasl2-dev \
  libsecret-1-0 \
  libssl-dev \
  openssl

ADD .npmrc ./iris.npmrc
COPY *.npmrc ./
RUN cat manual.npmrc > .npmrc || true; echo >> .npmrc; cat iris.npmrc >> .npmrc
ADD package.json .
ADD yarn.lock .
RUN yarn install --production --frozen-lockfile


############## Running container build stage:
FROM baseRunner as runner

# install native node_modules dependencies:
RUN apt-get update \
  && \
  apt-get install -y \
  libsasl2-2 \
  libsasl2-modules \
  libsecret-1-0 \
  openssl

# Create a directory as a mount point for credential files:
ENTRYPOINT ["node", "/app/App.js"]
ENV NODE_ENV=prod
ADD package.json .

## Instead of installing, we take node_modules from the runnerBuilder
COPY --from=runnerBuilder /app/node_modules /app/node_modules

# Bring compiled code from the builder container
COPY --from=builder /app/build /app
