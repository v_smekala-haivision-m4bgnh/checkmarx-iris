import uuid = require("uuid");
import { Container } from 'inversify';
import TYPES from './Types';
import { IHttpServer, HttpServer } from './HttpServer';
import { EventHubsClient } from "./event-hubs/EventHubsClient";
import { IEventHubsClient } from "./event-hubs/IEventHubsClient";
import { ConnectorService, IConnectorService } from "./connector/ConnectorService";

const container = new Container();
container.bind<IHttpServer>(TYPES.IHttpServer).to(HttpServer).inSingletonScope();
container.bind<string>(TYPES.InstanceId).toConstantValue(uuid.v4());
container.bind<IEventHubsClient>(TYPES.IEventHubsClient).to(EventHubsClient).inSingletonScope();
container.bind<IConnectorService>(TYPES.IConnectorService).to(ConnectorService).inSingletonScope();

export default container;
