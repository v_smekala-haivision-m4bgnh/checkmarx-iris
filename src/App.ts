import TYPES from './Types';
import container from './Inversify.config';
import { Config } from './Config';
import { IHttpServer } from './HttpServer';
import { logger, IKafkaClient } from '@hai/lf-node-common';
import { IEventHubsClient } from './event-hubs/IEventHubsClient';
import { IConnectorService } from './connector/ConnectorService';

export class App {
  constructor(private httpServer: IHttpServer,
              private eventHubsClient: IEventHubsClient,
              private connectorService: IConnectorService) {
    process.on('SIGINT', () => this.end('SIGINT'));
    process.on('SIGTERM', () => this.end('SIGTERM'));
  }

  public async main(): Promise<void> {
    try {
      logger.info('App -', `Starting up ${Config.APP_NAME}!`);
      this.connectorService.init();
      logger.info('App -', `Started up ${Config.APP_NAME} connector service!`);

      await this.httpServer.init();
      logger.info('App -', `Started up ${Config.APP_NAME} http server!`);
    } catch (err) {
      logger.error('App -', 'Init failed', err);
      process.exit(1);
    }
  }

  public async end(signal?: string): Promise<void> {
    if (signal) {
      logger.warn('App -', `${signal} received`);
    }
    await Promise.all([
      this.httpServer.stop(),
      this.connectorService.stop(),
      this.eventHubsClient.stop(),
    ]);
    process.exit(0);
  }
}

const httpServerInj = container.get<IHttpServer>(TYPES.IHttpServer);
const eventHubsClientInj = container.get<IEventHubsClient>(TYPES.IEventHubsClient);
const connectorServiceInj = container.get<IConnectorService>(TYPES.IConnectorService);

new App(httpServerInj, eventHubsClientInj, connectorServiceInj).main();
