import { Validator, IKafkaConsumerGroupOptions, IKafkaProducerOptions } from "@hai/lf-node-common";
import {QueueCreateOptions} from "@azure/storage-queue";

export interface IConnectorServiceConfig {
  connections: IConnectorServiceConnection[];
}

export interface IConnectorServiceConnection {
  disabled?: boolean;
  filter?: IJsonFilter;
  source: ISource;
  sink: ISink;
}

export interface IJsonFilter {
  type: 'JSON';
  // A hash of JSON paths to values to use as a filter predicate for messages.
  // Each message will first be JSON parsed.  The resulting JSON document will
  // then be evaluated by resolving each key in this hash as a JSON path and
  // comparing the resolved value against the value in the hash using strict
  // javascript equality (i.e. `===`).  If multiple JSON path keys are provided,
  // the resultant filter criteria is the logical AND of each of them.
  selector: Record<string, string | number | boolean>;
}

export interface ISource {
  id: string;
  type: SourceConnectionType;
  config: ISourceConfig;
}

export interface ISink {
  id: string;
  type: SinkConnectionType;
  config: ISinkConfig;
}

export enum SourceConnectionType {
  KAFKA = 'KAFKA',
  EVENTHUBS = 'EVENTHUBS',
  STORAGEQUEUE = 'STORAGEQUEUE',
}

export enum SinkConnectionType {
  KAFKA = 'KAFKA',
  EVENTHUBS = 'EVENTHUBS',
}

export interface ISourceConfig {
  dataId: string;
}

export interface IBrokerSourceConfig extends ISourceConfig {
  connString: string;
  consumerGroup: string;
}

export interface IKafkaSourceConfig extends IBrokerSourceConfig {
  options: IKafkaConsumerGroupOptions;
}

export interface IEventHubsSourceConfig extends IBrokerSourceConfig {
  storageConnString: string | undefined;
  containerName: string | undefined;
}

export interface IStorageQueueSourceConfig extends IBrokerSourceConfig{
  account: string;
  accessOptions: {accountKey?: string, sas?: string};
  pollingInterval?: number;
  queueCreateOptions?: QueueCreateOptions;
  encoding?: string;
  base64Encoded: boolean;
}

export interface ISinkConfig {
  dataId: string;
}

export interface IBrokerSinkConfig extends ISinkConfig {
  connString: string;
}

export interface IKafkaSinkConfig extends IBrokerSinkConfig {
  options: IKafkaProducerOptions;
}

export class ConnectorServiceConfig {
  public static validate(obj: IConnectorServiceConfig): IConnectorServiceConfig {
    Validator.checkIsObject(obj, 'config object is not valid');
    Validator.checkIsArray(obj.connections, 'connections is not a valid array');
    for (const conn of obj.connections) {
      // filter
      Validator.checkIsObject(conn.filter, 'filter object is not valid', { optional: true });
      if (conn.filter) {
        Validator.checkCondition(conn.filter.type === 'JSON', 'filter.type is not valid');
        Validator.checkIsObject(conn.filter.selector, 'filter.selector object is not valid');
      }

      // source
      Validator.checkIsObject(conn.source, 'source object is not valid');
      Validator.checkIsString(conn.source.id, 'source.id is not valid');
      Validator.checkIsType(conn.source.type, SourceConnectionType, 'source.type is not valid');
      Validator.checkIsObject(conn.source.config, 'source.config is not valid');
      Validator.checkIsString(conn.source.config.dataId, 'source.config.topic is not valid');
      Validator.checkIsString((conn.source.config as IBrokerSourceConfig).connString, 'source.config.connString is not valid', { optional: true });
      Validator.checkIsString((conn.source.config as IBrokerSourceConfig).consumerGroup, 'source.config.consumerGroup is not valid', { optional: true });
      Validator.checkIsString((conn.source.config as IEventHubsSourceConfig).storageConnString, 'source.config.storageConnString is not valid', { optional: true });
      Validator.checkIsString((conn.source.config as IEventHubsSourceConfig).containerName, 'source.config.containerName is not valid', { optional: true });
      Validator.checkIsObject((conn.source.config as IKafkaSourceConfig).options, 'source.config.options is not valid', { optional: true });

      // sink
      Validator.checkIsObject(conn.sink, 'source object is not valid');
      Validator.checkIsString(conn.sink.id, 'sink.id is not valid');
      Validator.checkIsType(conn.sink.type, SinkConnectionType, 'sink.type is not valid');
      Validator.checkIsObject(conn.sink.config, 'sink.config is not valid');
      Validator.checkIsString(conn.sink.config.dataId, 'sink.config.topic is not valid');
      Validator.checkIsString((conn.sink.config as IBrokerSinkConfig).connString, 'sink.config.connString is not valid', { optional: true });
      Validator.checkIsObject((conn.sink.config as IKafkaSinkConfig).options, 'sink.config.options is not valid', { optional: true });
    }

    return obj;
  }
}
