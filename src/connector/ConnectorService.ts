import {injectable} from 'inversify';
import * as fs from 'fs';
import { get } from 'lodash';
import {Config} from '../Config';
import {Config as CommonConfig, ConnectorSinkCallback, IConnectorSink, IConnectorSource, KafkaClient, logger} from '@hai/lf-node-common';
import {
  ConnectorServiceConfig,
  IBrokerSinkConfig,
  IConnectorServiceConfig,
  IConnectorServiceConnection,
  IEventHubsSourceConfig,
  IJsonFilter,
  IKafkaSinkConfig,
  IKafkaSourceConfig,
  ISink,
  ISource,
  IStorageQueueSourceConfig,
  SinkConnectionType,
  SourceConnectionType
} from './ConnectorModel';
import {EventHubsClient} from '../event-hubs/EventHubsClient';
import {StorageQueueClient} from "../azure-storage-queues/StorageQueueClient";


export interface IConnectorService {
  init(): void;
  stop(): Promise<void>;
}

@injectable()
export class ConnectorService implements IConnectorService {
  private readonly RETRY_TIMEOUT_MS = 5000;
  private config!: IConnectorServiceConfig;
  private sources: IConnectorSource[] = [];
  private sinks: IConnectorSink[] = [];

  public init(): void {
    this.getConfig();
    this.createConnections();
  }

  public async stop(): Promise<void> {
    try {
      await Promise.all([
        ...this.sources.map((source) => source.stop()),
        ...this.sinks.map((source) => source.stop()),
      ]);
      this.sources = [];
      this.sinks = [];
    } catch (err) {
      logger.error('ConnectorService -', 'Stop error', err);
    }
  }

  private async createConnections(): Promise<void> {
    logger.info('ConnectorService -', `Creating ${this.config.connections.length} connections ...`);
    for (const connection of this.config.connections) {
      this.createConnection(connection);
    }
  }

  private async createConnection(connection: IConnectorServiceConnection): Promise<void> {
    let source: IConnectorSource | undefined;
    let sink: IConnectorSink | undefined;

    if (connection.disabled) {
      logger.info('ConnectorService -', `Skipping Connecting source id=${connection.source.id} to sink id=${connection.sink.id}. Disabled`);
      return;
    }

    try {
      logger.info('ConnectorService -', `Connecting source id=${connection.source.id} to sink id=${connection.sink.id}`)
      sink = await this.createSink(connection.sink);
      source = await this.createSource(connection.source);
      await this.connect(source, sink, connection.filter);
      this.sources.push(source);
      this.sinks.push(sink);
    } catch (err) {
      logger.error('ConnectorService -', `Error creating connection: ${JSON.stringify(connection)}`, err);
      // Attempt to stop created source/sink before retrying
      source?.stop().catch();
      sink?.stop().catch();
      logger.info('ConnectorService -', `Retrying connection source id=${connection.source.id} to sink id=${connection.sink.id} ...`);
      setTimeout(() => { this.createConnection(connection); }, this.RETRY_TIMEOUT_MS);
    }
  }

  private async createSource(source: ISource): Promise<IConnectorSource> {
    let connSource: IConnectorSource;

    switch (source.type) {
      case SourceConnectionType.KAFKA:
        connSource = await this.createKafkaSource(source.config as IKafkaSourceConfig);
        break;
      case SourceConnectionType.EVENTHUBS:
        connSource = await this.createEventHubsSource(source.id, source.config as IEventHubsSourceConfig);
        break;
      case SourceConnectionType.STORAGEQUEUE:
        connSource = await this.createStorageQueueSource(source.id, source.config as IStorageQueueSourceConfig);
        break;
      default:
        throw new Error('Wrong source type=' + source.type);
    }

    connSource.setDataId(source.config.dataId);
    return connSource;
  }

  private async createKafkaSource(config: IKafkaSourceConfig): Promise<IConnectorSource> {
    const kafkaClient = new KafkaClient();
    await kafkaClient.init(config.connString, config.options);
    kafkaClient.initConsumerGroup(config.consumerGroup, [config.dataId], config.options);
    return kafkaClient;
  }

  private async createEventHubsSource(id: string, config: IEventHubsSourceConfig): Promise<IConnectorSource> {
    const eventHubs = new EventHubsClient();
    if (config.storageConnString && config.containerName) {
      eventHubs.initBalancedConsumer(id, config.consumerGroup, config.connString, config.dataId, config.storageConnString, config.containerName);
    } else {
      eventHubs.initConsumer(id, config.consumerGroup, config.connString, config.dataId);
    }
    return eventHubs;
  }

  private async createStorageQueueSource(id: string, config: IStorageQueueSourceConfig): Promise<IConnectorSource> {
    if (config.account && config.accessOptions) {
      const storageQueueClient = await StorageQueueClient.getStorageQueueClient(config.account, config.accessOptions);
      return storageQueueClient.initStorageQueue(config);
    } else {
      throw Error("'account' and 'accessOptions' configs are needed in order to init a storage queue client. Config: " +
          JSON.stringify(config));
    }
  }

  private async createSink(sink: ISink): Promise<IConnectorSink> {
    let connSink: IConnectorSink;

    switch (sink.type) {
      case SinkConnectionType.KAFKA:
        connSink = await this.createKafkaSink(sink.config as IKafkaSinkConfig);
        break;
      case SinkConnectionType.EVENTHUBS:
        connSink = await this.createEventHubsSink(sink.id, sink.config as IBrokerSinkConfig);
        break;
      default:
        throw new Error('Wrong sink type=' + sink.type);
    }

    connSink.setDataId(sink.config.dataId);
    return connSink;
  }

  private async createKafkaSink(config: IKafkaSinkConfig): Promise<IConnectorSink> {
    const kafkaClient = new KafkaClient();
    await kafkaClient.init(config.connString, config.options);
    return kafkaClient;
  }

  private async createEventHubsSink(id: string, config: IBrokerSinkConfig): Promise<IConnectorSink> {
    const eventHubs = new EventHubsClient();
    eventHubs.initProducer(id, config.connString, config.dataId);
    return eventHubs;
  }

  private async connect(source: IConnectorSource, sink: IConnectorSink, filter: IJsonFilter | undefined): Promise<void> {
    let cb = sink.getSinkCallback();
    if (filter) {
      cb = createFilterSinkCallback(filter, cb);
    }
    source.registerSink(cb);
  }

  private getConfig() {
    try {
      const configObj = JSON.parse(fs.readFileSync(Config.IRIS_CONFIG_PATH).toString());
      this.config = ConnectorServiceConfig.validate(configObj);
    } catch (err) {
      throw new Error('Error reading connector service config file - ' + err?.message);
    }
  }
}

function createFilterSinkCallback(filter: IJsonFilter, cb: ConnectorSinkCallback): ConnectorSinkCallback {
  return (key: string | undefined, data: string): void => {
    // message is actually the deserialized Buffer, can only proceed if it's an object
    const obj = data as any;
    if (typeof obj === "object") {
      const passes = Object.keys(filter.selector).reduce((acc, jsonPath) => {
        if (acc) {
          const value = get(obj, jsonPath);
          acc = value === filter.selector[jsonPath];
        }
        return acc;
      }, true);
      if (passes) {
        cb(key, data);
      }
    }
  };
}