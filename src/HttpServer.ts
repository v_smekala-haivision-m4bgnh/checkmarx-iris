import { injectable, inject, multiInject, optional } from 'inversify';
import 'reflect-metadata';
import * as bodyParser from 'body-parser';
import * as morgan from 'morgan';
import * as express from 'express';
import { Request, Response } from 'express';
import * as helmet from 'helmet';
import { Server } from 'http';
import { IRoute } from './IRoute';
import { Config } from './Config';
import { logger, sendBadRequestErrorResponse } from '@hai/lf-node-common';
import TYPES from './Types';

export interface IHttpServer {
	init(): Promise<Server>;
	stop(): Promise<void>;
	registerRoute(route: IRoute): void;
}

@injectable()
export class HttpServer implements IHttpServer {
	private app: express.Express;
	private server!: Server;
	private routes: IRoute[] = [];
	private port: number = Config.HTTP_PORT;

	public constructor(@inject(TYPES.InstanceId) private instanceId: string,
										 @multiInject(TYPES.IRoute) @optional() private registrableRoutes: IRoute[]) {
		this.app = express();
	}

	public init(): Promise<Server> {
		return new Promise<Server>(async (resolve, reject) => {
			this.app.use(bodyParser.json());
			this.app.use((error: any, req: Request, res: Response, next: () => void) => {
				if (error instanceof SyntaxError) {
					sendBadRequestErrorResponse(res, 'Invalid JSON')
				} else {
					next();
				}
			});
			this.app.use(morgan('combined', {
				stream: { write: message => logger.debug(message.trim()) },
			}));
			this.app.use(helmet());

			if (process.env.NODE_ENV === 'dev') {
				this.setDevOptions();
			}

			this.app.get(['/', '/health'], (req: express.Request, res: express.Response): void => {
				res.send(this.getHealth());
			});

			// Registrable routes
			this.registrableRoutes.forEach( route => this.registerRoute(route));

			this.server = this.app.listen(this.port, () => {
				logger.info('HttpServer -', `Express listening on port ${this.port}`);
				resolve(this.server);
			}).on('error', (err) => {
				reject(err);
			});
		});
	}

	public registerRoute(route: IRoute): void {
		logger.info('HttpServer -', `Registering route ${route.path()}`);
		this.app.use(route.path(), route.getRequestHandlers(), route.create(this.server));
		this.routes.push(route);
	}

	public stop(): Promise<void> {
		logger.warn('HttpServer -', `Stopping server`);
		this.routes.forEach((route) => route.destroy());

		return new Promise<void>(async (resolve, reject) => {
			if (this.server) {
				this.server.close(() => {
					resolve();
				});
			} else {
				resolve();
			}
		});
	}

	private getHealth(): any {
		return {
			app: Config.APP_NAME,
			instanceId: this.instanceId,
			status: 'ok',
		};
	}

	private setDevOptions(): void {
		// this.port =  Utils.getRandomPort();
		this.app.options('/*', (req, res, next) => {
			res.header('Access-Control-Allow-Origin', '*');
			res.header('Access-Control-Allow-Headers', '*, authorization');
			res.header('Access-Control-Allow-Methods', '*');
			next();
		});

		this.app.get('/*', (req, res, next) => {
			res.header('Access-Control-Allow-Origin', '*');
			next();
		});

		this.app.delete('/*', (req, res, next) => {
			res.header('Access-Control-Allow-Origin', '*');
			next();
		});

		this.app.post('/*', (req, res, next) => {
			res.header('Access-Control-Allow-Origin', '*');
			next();
		});

		this.app.put('/*', (req, res, next) => {
			res.header('Access-Control-Allow-Origin', '*');
			next();
		});
	}
}
