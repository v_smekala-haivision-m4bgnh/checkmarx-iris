import { COMMONTYPES } from "@hai/lf-node-common";

const TYPES = {
	IRoute: Symbol('IRoute'),
	IDBClient: COMMONTYPES.IDBClient,
	IKafkaClient: COMMONTYPES.IKafkaClient,
	IExternalKafkaClient: Symbol('IExternalKafkaClient'),
	IHttpServer: Symbol('IHttpServer'),
	IAuthService: Symbol('IAuthService'),
	IAuthRepo: Symbol('IAuthRepo'),
	InstanceId: Symbol('InstanceId'),
	IEventHubsClient: Symbol('IEventHubsClient'),
	IBrokerRoute: Symbol('IBrokerRoute'),
	IConnectorService: Symbol('IConnectorService'),
};

export default TYPES;
