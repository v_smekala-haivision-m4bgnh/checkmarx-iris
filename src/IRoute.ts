import { Server } from 'http';
import { IRouter, RequestHandler } from "express";

export interface IRoute {
	path(): string;
	create(server: Server): IRouter<IRoute>;
	getRequestHandlers(): RequestHandler[];
	destroy(): void;
}
