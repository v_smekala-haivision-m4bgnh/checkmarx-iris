export function uniq<T>(listWithRepetitions: T[]): T[] {
	return [...new Set(listWithRepetitions)];
}

export function flatten<T>(listofLists: T[][]): T[] {
	const empty: T[] = [];
	return empty.concat(...listofLists);
}

export function promiseMap<T, S>(list: T[], mapFunc: (elem: T) => Promise<S>): Promise<S[]> {
	return Promise.all(list.map(elem => mapFunc(elem)));
}