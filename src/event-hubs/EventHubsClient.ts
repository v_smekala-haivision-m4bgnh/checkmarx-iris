import { injectable } from 'inversify';
import 'reflect-metadata';
import { EventHubProducerClient, EventHubConsumerClient, Subscription } from '@azure/event-hubs';
import { ContainerClient } from '@azure/storage-blob';
import { BlobCheckpointStore } from '@azure/eventhubs-checkpointstore-blob';

import { IEventHubsClient, EventHubsConsumerCallback } from './IEventHubsClient';
import { Serializer, JSONSerializer, logger, IConnectorSource, IConnectorSink, ConnectorSinkCallback } from '@hai/lf-node-common';

@injectable()
export class EventHubsClient implements IEventHubsClient, IConnectorSource, IConnectorSink {
  public connectorDataId: string = '';

  private producers: Map<string, EventHubProducerClient> = new Map();
  private hubProducers: Map<string, string[]> = new Map();
  private consumers: EventHubConsumerClient[] = [];
  private subscriptions: Subscription[] = [];
  private consumerListeners: Map<string, EventHubsConsumerCallback[]> = new Map();
  private sourceListeners: Map<string, ConnectorSinkCallback[]> = new Map();

  public initProducer(producerId: string, connString: string, eventHubName: string): EventHubProducerClient {
    const producer = new EventHubProducerClient(connString, eventHubName);
    this.producers.set(producerId, producer);
    let producersForHub = this.hubProducers.get(eventHubName);
    if (!producersForHub) {
      producersForHub = [];
      this.hubProducers.set(eventHubName, producersForHub);
    }
    producersForHub.push(producerId);
    logger.info('EventHubsClient -', `Created producer id=${producerId}`);
    return producer;
  }

  public initConsumer(consumerId: string, consumerGroup: string, connString: string, eventHubName: string, checkpointStore?: BlobCheckpointStore): EventHubConsumerClient {
    const consumerClient = checkpointStore ?
      new EventHubConsumerClient(consumerGroup, connString, eventHubName, checkpointStore) :
      new EventHubConsumerClient(consumerGroup, connString, eventHubName);
    this.subscriptions.push(this.subscribeConsumer(consumerId, consumerClient));
    this.consumers.push(consumerClient);
    logger.info('EventHubsClient -', `Created consumer id=${consumerId}`);
    return consumerClient;
  }

  public async initBalancedConsumer(consumerId: string, consumerGroup: string, connString: string, eventHubName: string, storageConnString: string, containerName: string): Promise<EventHubConsumerClient> {
    const blobContainerClient = new ContainerClient(storageConnString, containerName);
    const checkpointStore = new BlobCheckpointStore(blobContainerClient);
    return this.initConsumer(consumerId, consumerGroup, connString, eventHubName, checkpointStore);
  }

  public async sendMessage(producerId: string, message: any, serializer: Serializer = JSONSerializer): Promise<void> {
    const left = await this.sendMessages(producerId, [message], serializer);
    if (left.length > 0) {
      // This might happen if the message exceeds the batch size
      throw new Error('Message could not be added to the batch');
    }
  }

  public async sendMessages(producerId: string, messages: any[], serializer: Serializer = JSONSerializer): Promise<any[]> {
    if (messages.length === 0) {
      return [];
    }

    try {
      const producer = this.getProducer(producerId);
      const eventDataBatch = await producer.createBatch();    

      // Messages are added until the batch is full
      while (messages.length > 0) {
        const added = eventDataBatch.tryAdd({ body: serializer(messages[0]) });
        if (!added) {
          break;
        } else {
          messages = messages.slice(1);
        }
      }

      logger.debug('EventHubsClient -', `Producer id=${producerId} eventHubName=${producer.eventHubName}, sending ${eventDataBatch.count} messages, size: ${eventDataBatch.sizeInBytes} bytes`);
      await producer.sendBatch(eventDataBatch);
      // Messages that didn't fit the batch are returned
      return messages;
    } catch (err) {
      logger.error('EventHubsClient -', `Error sending message(s) to producer=` + producerId, err);
      throw err;
    }
  }

  public registerEventHubListener(consumerId: string, listener: EventHubsConsumerCallback): void {
    this.registerListener(consumerId, this.consumerListeners, listener);
  }

  public unregisterEventHubListener(consumerId: string, listener: EventHubsConsumerCallback): void {
    this.unregisterListener(consumerId, this.consumerListeners, listener);
  }

  public async stop(): Promise<void> {
    logger.warn('EventHubsClient -', 'Closing down');
    try {
      await Promise.all([
        [...this.producers.values()].map((producer) => producer.close()),
        this.subscriptions.map((subscription) => subscription.close()),
        this.consumers.map((consumer) => consumer.close())
      ]);
    } catch (err) {
      logger.error('EventHubsClient -', 'Close down error', err);
    }
  }

  // IConnectorSource, IConnectorSink
  public setDataId(id: string): void {
    this.connectorDataId = id;
  }

  // IConnectorSource
  public registerSink(sinkCallback: ConnectorSinkCallback): void {
    this.registerListener(this.connectorDataId, this.sourceListeners, sinkCallback);
  }

  // IConnectorSource
  public unregisterSink(sinkCallback: ConnectorSinkCallback): void {
    this.unregisterListener(this.connectorDataId, this.sourceListeners, sinkCallback);
  }

  // IConnectorSink
  public getSinkCallback(serializer?: Serializer): ConnectorSinkCallback {
    return (key: string | undefined, data: any): void => {
      // get producers for this event hub
      const producerIds = this.hubProducers.get(this.connectorDataId);
      if (producerIds) {
        Promise.all(producerIds.map((producerId) => this.sendMessage(producerId, data, serializer)))
          .catch((err) => {
            logger.error('EventHubClient -', 'Producer error', err);
          });
      }
    };
  }

  private getProducer(producerId: string): EventHubProducerClient {
    const producer = this.producers.get(producerId);
    if (!producer) {
      throw new Error('No event hub producer found for id=' + producerId);
    }
    return producer;
  }

  private subscribeConsumer(consumerId: string, consumer: EventHubConsumerClient): Subscription {
    return consumer.subscribe({
      processEvents: async (events, context) => {
        const consumerListeners = this.consumerListeners.get(consumerId) || [];
        const sourceListeners = this.sourceListeners.get(consumerId) || [];
        for (const event of events) {

          logger.debug('EventHubsClient -', 'Event', JSON.stringify(event));

          logger.debug('EventHubsClient -', `Received message consumer_id=${consumerId}, partition=${context.partitionId}, consumer_group=${context.consumerGroup}, body=${JSON.stringify(event.body)}`);
          consumerListeners.forEach((listener) => listener(event.body));
          sourceListeners.forEach((listener) => listener(undefined, event.body));
        }
        await context.updateCheckpoint(events[events.length - 1]);
      },

      processError: async (err, context) => {
        logger.error('EventHubsClient -', `Error on consumer_id=${consumerId}, partition=${context.partitionId}, consumer group=${context.consumerGroup}`, err);
      },
    });
  }

  public registerListener(consumerId: string, listenersMap: Map<string, any[]>, listener: any): void {
    if (!listenersMap.has(consumerId)) {
      listenersMap.set(consumerId, []);
    }
    listenersMap.get(consumerId)!.push(listener);
  }

  public unregisterListener(consumerId: string, listenersMap: Map<string, any[]>, listener: any): void {
    if (listenersMap.has(consumerId)) {
      listenersMap.set(consumerId, listenersMap.get(consumerId)!.filter((elem) => elem !== listener));
    }
  }
}