import { Serializer } from "@hai/lf-node-common";
import { EventHubConsumerClient, EventHubProducerClient } from "@azure/event-hubs";

export type EventHubsConsumerCallback = (data: any) => void;

export interface IEventHubsClient {
  initProducer(producerId: string, connString: string, eventHubName: string): EventHubProducerClient;
  sendMessage(producerId: string, message: any, serializer?: Serializer): Promise<void>;
  sendMessages(producerId: string, messages: any[], serializer?: Serializer): Promise<any[]>;
  initConsumer(consumerId: string, consumerGroup: string, connString: string, eventHubName: string): EventHubConsumerClient;
  initBalancedConsumer(consumerId: string, consumerGroup: string, connString: string, eventHubName: string, storageConnString: string, containerName: string): Promise<EventHubConsumerClient>;
  registerEventHubListener(consumerId: string, listener: EventHubsConsumerCallback): void;
  unregisterEventHubListener(consumerId: string, listener: EventHubsConsumerCallback): void;
  stop(): Promise<void>;
}