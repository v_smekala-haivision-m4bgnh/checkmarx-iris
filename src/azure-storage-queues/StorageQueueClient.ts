import {createLogger, IConnectorSource} from "@hai/lf-node-common";
import {QueueClient, QueueItem, QueueServiceClient, StorageSharedKeyCredential} from "@azure/storage-queue";
import {Logger} from "winston";
import {IStorageQueueSourceConfig} from "../connector/ConnectorModel";
import Timer = NodeJS.Timer;

export interface IStorageQueueClient {
    initStorageQueue(config: IStorageQueueSourceConfig): Promise<IConnectorSource>;
}

export class StorageQueueClient implements IStorageQueueClient {
    private queueServiceClient: QueueServiceClient | undefined;
    private static clientMap = new Map<string, StorageQueueClient>();
    private logger: Logger;

    public static async getStorageQueueClient(account: string, accessOptions: {accountKey?: string, sas?: string}): Promise<StorageQueueClient> {
        let storageQueueClient = this.clientMap.get(account);
        if (!storageQueueClient) {
            storageQueueClient = new StorageQueueClient(account);
            this.clientMap.set(account, storageQueueClient);
            await storageQueueClient.initStorageClient(accessOptions);
        }
        return storageQueueClient;
    }

    constructor(private account: string) {
        this.logger = createLogger(`StorageQueueClient[${this.account}]`);
    }

    private async initStorageClient(accessOptions: {accountKey?: string, sas?: string}) {
        if (accessOptions.accountKey) {
            // Use StorageSharedKeyCredential with storage account and account key
            // StorageSharedKeyCredential is only available in Node.js runtime, not in browsers
            const sharedKeyCredential = new StorageSharedKeyCredential(this.account, accessOptions.accountKey);
            const url = `https://${this.account}.queue.core.windows.net`;
            this.queueServiceClient = new QueueServiceClient(
                url,
                sharedKeyCredential,
                {
                    retryOptions: {maxTries: 4}, // Retry options
                }
            );
        } else if (accessOptions.sas) {
            this.queueServiceClient = new QueueServiceClient(
                `https://${this.account}.queue.core.windows.net${accessOptions.sas}`,
            );
        }

        if (this.queueServiceClient === undefined) {
            throw Error("Doesn't have access credentials to create queue");
        } else {
            this.logger.info("Created Storage Queue client");
        }
    }

    public async initStorageQueue(config: IStorageQueueSourceConfig): Promise<IConnectorSource> {
        if (this.queueServiceClient) {
            const queueItem = await StorageQueueClient.initQueue(config, this.queueServiceClient);
            return new StorageQueueConnector(queueItem, this.queueServiceClient, config);
        } else {
            throw Error("Queue Service client not initialized, cannot create Storage Queue Connector");
        }
    }

    private static async initQueue(config: IStorageQueueSourceConfig, queueServiceClient: QueueServiceClient): Promise<QueueItem> {
        const queueClient = queueServiceClient.getQueueClient(config.dataId);
        const response = await queueClient.create(config.queueCreateOptions);
        if (response.errorCode !== undefined) {
            throw new Error(`Error creating queue. Error code ${response.errorCode}`);
        }
        return this.findQueue(config.dataId, queueServiceClient);
    }

    private static async findQueue(queueName: string, queueServiceClient: QueueServiceClient) {
        const iter1 = queueServiceClient.listQueues();
        let i = 1;
        for await (const item of iter1) {
            if (item.name === queueName) {
                return item;
            }
            i++;
        }
        throw new Error(`Queue ${queueName} not found`);
    }

}

class StorageQueueConnector implements IConnectorSource {
    public queueName?: string;
    private queueClient?: QueueClient;
    private running: boolean = false;
    private logger: Logger;
    private sinkCallbacks: Array<(key: (string | undefined), data: string) => void> = [];
    private interval: Timer | undefined;
    private iterationRunning?: Promise<void>;

    constructor(private queue: QueueItem, private queueServiceClient: QueueServiceClient, private config: IStorageQueueSourceConfig) {
        this.logger = createLogger(`StorageQueueConnector[${this.queueServiceClient.accountName}/${queue.name}]`);
    }

    public registerSink(sinkCallback: (key: (string | undefined), data: string) => void): void {
        this.sinkCallbacks.push(sinkCallback);
    }

    public setDataId(dataId: string): void {
        this.logger.debug(`Data Id set: ${dataId}`);
        this.queueName = dataId;
        this.queueClient = this.queueServiceClient.getQueueClient(this.queueName);
        this.running = true;
        this.receiveLoop();
    }

    public async stop(): Promise<void> {
        if (this.interval) {
            clearInterval(this.interval);
            this.running = false;
            if (this.iterationRunning) {
                return this.iterationRunning;
            }
        }
    }

    private receiveLoop() {
        const pollingInteval = this.config.pollingInterval ?? 5000;
        this.logger.info(`Init receive loop with polling interval ${pollingInteval}ms`);
        this.interval = setInterval(async () => {
            if (this.queueClient) {
                this.iterationRunning = this.step(this.queueClient);
            } else {
                this.logger.debug("Not started");
            }
        }, pollingInteval);
    }

    private async step(queueClient: QueueClient) {
        this.logger.debug("Still running");
        const response = await queueClient.receiveMessages();
        if (response.receivedMessageItems.length === 1) {
            const receivedMessageItem = response.receivedMessageItems[0];
            const messageText = this.config.base64Encoded ? this.decodeBase64(receivedMessageItem.messageText) : receivedMessageItem.messageText;
            this.logger.debug(`Processing message with content: ${messageText} to ${this.sinkCallbacks.length} sinks`);
            this.sinkCallbacks.forEach(sinkCallback => {
                try {
                    sinkCallback(receivedMessageItem.messageId, messageText)
                } catch (e) {
                    this.logger.error("Error processing message in callback");
                }
            });
            this.logger.debug(`Deleting message`);
            const deleteMessageResponse = await queueClient.deleteMessage(
                receivedMessageItem.messageId,
                receivedMessageItem.popReceipt
            );
            this.logger.debug(
                `Delete message successfully, service assigned request Id: ${deleteMessageResponse.requestId}`
            );
        }
        this.iterationRunning = undefined;
    }

    private decodeBase64(receivedMessage: string) {
        return Buffer.from(receivedMessage, 'base64').toString(this.config.encoding ?? 'utf-8');
    }

    public unregisterSink(sinkCallback: (key: (string | undefined), data: string) => void): void {
        this.sinkCallbacks = this.sinkCallbacks.filter( sc => sc !== sinkCallback);
    }


}
