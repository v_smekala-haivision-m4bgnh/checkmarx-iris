import { Config as CommonConfig } from "@hai/lf-node-common";

export class Config {
  public static APP_NAME = 'iris';
  public static HTTP_PORT = 17140;

  public static IRIS_CONFIG_PATH = process.env.IRIS_CONFIG_PATH || CommonConfig.BASE_VOLUME_PATH + '/iris_config.json';
}
