import 'jest';
import { ConnectorServiceConfig } from '../../src/connector/ConnectorModel';

describe('Connector model tests', () => {
	it('Should correctly validate models', () => {
		let obj: any;
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = { connections: [] };
		ConnectorServiceConfig.validate(obj);

		obj = { connections: {} };
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = { connections: [{}] };
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'KAFKA',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
					}
				}
			}]
		};
		ConnectorServiceConfig.validate(obj);

		obj = {
			connections: [{
				source: {
					// <-
					type: 'KAFKA',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
					}
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'KAFKA',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
					}
				},
				sink: {
					// <-
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
					}
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'WRONG_TYPE', // <-
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
					}
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'KAFKA',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
					}
				},
				sink: {
					id: 'sinkId',
					type: 'OTHER_WRONG', // <-
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
					}
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'KAFKA',
					// <-
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
					}
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'KAFKA',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					// <-
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'KAFKA',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						// <-
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
					}
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'KAFKA',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						// <-
					}
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'EVENTHUBS',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
						storageConnString: 'storage_conn_string',
						containerName: 'container_name',
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
					}
				}
			}]
		};
		ConnectorServiceConfig.validate(obj);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'EVENTHUBS',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
						storageConnString: 'storage_conn_string',
						containerName: {}, // <-
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
					}
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'EVENTHUBS',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
						storageConnString: 1234, // <-
						containerName: 'container_name',
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
					}
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'KAFKA',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
						options: {},
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
						options: {},
					}
				}
			}]
		};
		ConnectorServiceConfig.validate(obj);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'KAFKA',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
						options: 'options', // <-
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
					}
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);

		obj = {
			connections: [{
				source: {
					id: 'sourceId',
					type: 'KAFKA',
					config: {
						connString: "Endpoint=sb://eventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=1234",
						consumerGroup: 'consumer_group',
						dataId: 'topic_source',
					}
				},
				sink: {
					id: 'sinkId',
					type: 'KAFKA',
					config: {
						connString: 'kafka:9092',
						dataId: 'topic_sink',
						options: 'options', // <-
					}
				}
			}]
		};
		expect(() => ConnectorServiceConfig.validate(obj)).toThrow(Error);
	});
});