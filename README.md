# Iris

> Iris is the personification and goddess of the rainbow and messenger of the gods.

## Development

To develop the micro service you can launch node app using telepresence so it is in a virtual network inside your development cluster:

```bash
npm run telepresence
```

Contents of `telepresence` task:

```bash
telepresence -s lightflow-iris --run npm run dev
```

This command will create a telepresence shell, swap lightflow-iris with a virtual pod that will redirect to your localhost and then will launch `npm run dev`

Contents of `dev` task:

```bash
NODE_ENV=dev nodemon
```

This, at the same time, uses nodemon.json to specify what to launch:

```json
{
  "watch": ["src"],
  "ext": "ts",
  "ignore": ["src/**/*.spec.ts"],
  "exec": "node -r ts-node/register --inspect ./src/App.ts"
}
```

This files specifies which files to watch and will execute node with `ts-node` as registered compiler. `ts-node`
will compile and execute all the typescript files, so you don't need to first run build or any other command. This will also enable debug hook (--inspect). You can connect to the debugger using intellij creating a `Attach to node.js/Chrome...` configuration and setting port to `9229`.
